# Installation d'un projet Angular

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 17.2.1.

## Installer un projet Angular à partir de la ligne de commande
Commencez par ouvrir un terminal sur votre ordinateur, puis entrez la commande suivante: 
````
npm install -g @angular/cli
````
Félicitations !

## Créer un espace de travail 
Pour créer un nouvel espace de travail ANgular, entrez dans le terminal la ligne de commande suivante:
````
ng new new-app
````
La commande `ng new` vous demande des informations sur les fonctionnalités de votre applications. Vous pouvez accepter les valeurs par défaut (touche Entrée ou retour).

L'interface de programmation Angular va ensuite installer les paquets npm Angular et dépendances nécessaires. Leur chargement peut prendre quelques minutes. 
Refaites-vous un café en attendant !

C'est bon ! Un nouvel espace de travail et une application de bienvenue sont créés, vous pouvez commencer à travailler ! 

N'oubliez pas la commande `cd new-app`quand même.

## Serveur de développement

Lancez la commance `ng serve` pour démarrer un nouveau serveur. Naviguez sur `http://localhost:4200/`. L'application se rechargera automatiquement si vous modifiez l'un des fichiers sources.

# Pour aller plus loin

## Echafaudage de code 

Lancez `ng generate component component-name` pour générer un nouveau composant. Il est également possible d'utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Construire le projet

La commande `ng build` permet de construire le projet. Les artéfacts de construction seront stockés dans le répertoire `dist/`.

## Exécuter les tests unitaires

Lancez `ng test` pour exécuter les tests via [Karma](https://karma-runner.github.io).

## Exécuter les tests de bout en bout

La commande `ng e2e` permet d'exécuter les tests de bout en bout via la plateforme de votre choix. Pour utiliser cette commande, vous devez d'abord ajouter un paquet qui implémente les capacités de test de bout en bout.

## Aide additionnelle

Pour obtenir plus d'aide sur la CLI Angular, utilisez `ng help` ou allez voir la page [Angular CLI Overview and Command Reference](https://angular.io/cli).
